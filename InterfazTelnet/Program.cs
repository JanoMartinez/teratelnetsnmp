﻿using System.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
//using System.Windows.Forms;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MinimalisticTelnet;
using System.Text.RegularExpressions;
using SnmpSharpNet;
using System.Net;

namespace InterfazTelnet
{
    class Program
    {

        public System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        public List<OLT> listaOLT;
        public Dictionary<string, TelnetConnection> listaConexiones = new Dictionary<string, TelnetConnection>();
        public List<ComandoProcesado> comandosProcesados = new List<ComandoProcesado>();
        public bool procesando = false; 

        /// <summary>
        /// Función Principal para el Inicio de la Interfaz
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Program obj = new Program();
            //obj.Start();

            Console.ReadLine();
        }

        /// <summary>
        /// Constructor, inicializa el timer y obtiene las OLT almacenadas en la tabla de GENERALS para el acceso por Telnet
        /// </summary>
        public Program()
        {
            DBFuncion db = new DBFuncion();
            SqlDataReader reader = db.consultaReader("CONGENERALS");
            listaOLT = db.MapDataToEntityCollection<OLT>(reader).ToList();
            reader.Close();
            db.conexion.Close();

            foreach (OLT oltAux in listaOLT)
            {
                TelnetConnection tc = new TelnetConnection(oltAux.Ip, 23);
                //TelnetConnection tc = new TelnetConnection(oltAux.Ip, 9878);
                string s = tc.Login(oltAux.User_, oltAux.Pass, 1000);
                Console.WriteLine(oltAux.Ip);
                Console.WriteLine(s);
                listaConexiones.Add(oltAux.Ip, tc);
            }
            Console.ReadLine();
            //while (true)
            //{
            //    ProcesaCNR();
            //    System.Threading.Thread.Sleep(15000);
            //}

        }

        private void TimerCallback(Object o)
        {
            ProcesaCNR();

        }

        /// <summary>
        /// Obtiene los datos necesarios para procesos los comandos y ejecuta el procedimiento correspondiente dependiendo del
        /// tipo de comando
        /// </summary>
        private void ProcesaCNR()
        {
            
                Console.WriteLine("Tick...");
            
                Console.WriteLine("Obteniendo comandos...");
                comandosProcesados = new List<ComandoProcesado>();

                List<Comandos> comandos = new List<Comandos>();
                DBFuncion dbComandos = new DBFuncion();
                dbComandos.agregarParametro("@Op", System.Data.SqlDbType.Int, 0);
                SqlDataReader reader = dbComandos.consultaReader("ProcesaCNR_ONUTelnet");
                comandos = dbComandos.MapDataToEntityCollection<Comandos>(reader).ToList();
                foreach (Comandos comandoAux in comandos)
                {
                    
                    //dar formato despues a la mac -- dimitri
                    comandoAux.MacAddress = comandoAux.MacAddress.Substring(0, 2) + " " + comandoAux.MacAddress.Substring(2, 2) + " " + comandoAux.MacAddress.Substring(4, 2) + " " + comandoAux.MacAddress.Substring(6, 2) + " " + comandoAux.MacAddress.Substring(8, 2) + " " + comandoAux.MacAddress.Substring(10, 2);


                    //ThreadStart starter;
                    //Thread aux;
                    if (comandoAux.tipserv == 2)
                    {
                        switch (comandoAux.Comando)
                        {
                            case "activar":
                                activar(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Velocidad_Subida, comandoAux.Velocidad_Bajada, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "reactivar":
                                InternetReactivar(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "suspender":
                                InternetSuspender(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "borrar":
                                InternetSuspender(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "borrartodo":
                                BorrarTodo(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "reset":
                                Reset(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "cam_paq"://cambiar velocidad
                                InternetCambioPaquete(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Velocidad_Subida, comandoAux.Velocidad_Bajada, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "campaq":
                                InternetCambioPaquete(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Velocidad_Subida, comandoAux.Velocidad_Bajada, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                        }
                    }
                    else if (comandoAux.tipserv == 1 || comandoAux.tipserv == 3)
                    {
                        switch (comandoAux.Comando)
                        {
                            case "activar":
                                activarTV(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP, comandoAux.Whitelist);
                                break;
                            case "reactivar":
                                //TVReactivarTelnet(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP, comandoAux.Whitelist);
                                TVReactivar(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP, comandoAux.Whitelist);
                                break;
                            case "suspender":
                                TVSuspender(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP, comandoAux.Whitelist);
                                break;
                            case "borrar":
                                TVSuspender(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP, comandoAux.Whitelist);
                                break;
                            case "borrartodo":
                                BorrarTodo(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "reset":
                                Reset(comandoAux.MacAddress, comandoAux.Contrato, comandoAux.Consecutivo, comandoAux.Puerto, comandoAux.NoOnu, comandoAux.IP);
                                break;
                            case "campaq":
                                TVCambioPaquete(comandoAux.Consecutivo, comandoAux.MacAddress);
                                break;
                        }
                    }
                }
                reader.Close();
                dbComandos.conexion.Close();
        }

        /// <summary>
        /// Procedimiento correspondiente al comando activar, crea un hilo por cada OLT, los hilos ejecutan los pasos
        /// correspondientes para el comando activar por cada OLT
        /// </summary>
        /// <param name="MAC"></param>
        private void activar(string MAC, int Contrato, long Consecutivo, string VelocidadSubida, string VelocidadBajada, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
             if (NumeroOLT > 0)
            {
                InternetActivarconIPTelnet(MAC, Contrato, Consecutivo, VelocidadSubida, VelocidadBajada, NumeroOLT, NumeroONU, IPOLT_ONU);
            }
            else
            {
                List<Thread> threadsOLT = new List<Thread>();
                foreach (OLT olt in listaOLT)
                {
                    Console.WriteLine("olt: " + olt.Ip);
                    InternetActivar(olt.Ip, olt.User_, olt.Pass, MAC, Contrato.ToString(), Consecutivo, VelocidadSubida, VelocidadBajada);               
                }
            }
        }

        /// <summary>
        /// Procedimiento correspondiente al comando activar, crea un hilo por cada OLT, los hilos ejecutan los pasos
        /// correspondientes para el comando activar por cada OLT
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void activarTV(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            if (NumeroOLT > 0)
            {
                TVActivarSoloHabilitaConIPTelnet(MAC, Contrato, Consecutivo, NumeroOLT, NumeroONU, IPOLT_ONU, Whitelist);
            }
            else
            {
                List<Thread> threadsOLT = new List<Thread>();
                foreach (OLT olt in listaOLT)
                {
                    Console.WriteLine("olt: " + olt.Ip);
                    TVActivar(olt.Ip, olt.User_, olt.Pass, MAC, Contrato.ToString(), Consecutivo); 
                }
            }
        }

        /// <summary>
        /// Accede por Telnet a la OLT correspondiente, obtiene el listado de las ONU ilegales, las compara con la MAC
        /// solicitada, de haber coincidencia se obtiene el status y posición de la ONU en la OLT
        /// </summary>
        /// <param name="Ip"></param>
        /// <param name="User_"></param>
        /// <param name="Pass"></param>
        /// <param name="MAC"></param>
        private void InternetActivar(string Ip, string User_, string Pass, string MAC, string Contrato, long Consecutivo, string VelocidadSubida, string VelocidadBajada)
        {
            try
            {

                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdIlegales(MAC,Ip).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    Console.WriteLine("No se encontro en la lista de ilegales en la olt: " + Ip);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la lista de ilegales en la olt: " + Ip);
                    db2.consultaSinRetorno("ActualizaComandos");
                    //no se encuentra la mac en esta lista de ilegales
                    return;
                }
                else
                {
                    Console.WriteLine("SI se encontro en la lista de ilegales en la olt: " + Ip);
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, Ip), ContarMacWhiteList(BuscarIdIlegales(MAC, Ip), Ip), Ip);//mac, whitelist, numero de nuevo registro, ip
                    Console.WriteLine("Mac agregada a la Whitelist");

                    string macadresstelnet = Regex.Replace(MAC.Replace(" ", ""), "(.{2})(?!$)", "$0-").ToLower();

                    String[] position = sendCommand(listaConexiones[Ip], "show onu-position " + macadresstelnet);
                    foreach (String pos in position)
                    {
                        if (pos.Contains("Found ONU"))
                        {
                            String[] posSep = { "(", ")" };
                            String[] posAux = pos.Split(posSep, StringSplitOptions.RemoveEmptyEntries);
                            Console.WriteLine(posAux[1]);

                            String[] portPosSep = { "-" };
                            String[] portPos = posAux[1].Split(portPosSep, StringSplitOptions.RemoveEmptyEntries);
                            string puerto = portPos[1];
                            string posicion = portPos[2];

                            if (posAux[2].Contains("online"))
                            {
                                /// Aqui va el codigo cuando está online
                                String[] sep3 = { "-", ")" };
                                String[] NumeroONUAux = pos.Split(sep3, StringSplitOptions.RemoveEmptyEntries);

                                sendCommand(listaConexiones[Ip], "olt " + NumeroONUAux[1]);
                                sendCommand(listaConexiones[Ip], "onu " + NumeroONUAux[2]);
                                //sendCommand(listaConexiones[IPOLT_ONU], "catv disable");
                                sendCommand(listaConexiones[Ip], "link 1");
                                sendCommand(listaConexiones[Ip], "sla upstream 0 1 " + VelocidadSubida + " 1");
                                sendCommand(listaConexiones[Ip], "sla downstream " + VelocidadBajada + " 512 1");
                                sendCommand(listaConexiones[Ip], "exit");
                                sendCommand(listaConexiones[Ip], "save");
                                sendCommand(listaConexiones[Ip], "exit");
                                sendCommand(listaConexiones[Ip], "exit");
                                sendCommand(listaConexiones[Ip], "system save all");

                                DBFuncion db2 = new DBFuncion();
                                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                                db2.consultaSinRetorno("ActualizaComandos");

                                DBFuncion db1 = new DBFuncion();
                                db1.agregarParametro("@Contrato", System.Data.SqlDbType.BigInt, Contrato);
                                db1.agregarParametro("@olt", System.Data.SqlDbType.VarChar, 1);
                                db1.agregarParametro("@onu", System.Data.SqlDbType.Int, 1);
                                db1.agregarParametro("@Vlan", System.Data.SqlDbType.Int, Global.VLan);
                                db1.agregarParametro("@IpOlt", System.Data.SqlDbType.VarChar, Ip);
                                db1.agregarParametro("@Consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                                db1.agregarParametro("@Whitelist", System.Data.SqlDbType.Bit, 1);
                                db1.consultaSinRetorno("Actualiza_PosicionOnu");
                                return;
                            }
                            else
                            {
                                RemoveMacToWhiteList(BuscarIdWhiteList(MAC, Ip), BuscarIdWhiteListBorrar(MAC, Ip), Ip);
                            }
                        }
                    }

                    

                    //DBFuncion db2 = new DBFuncion();
                    //db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 0);
                    //db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    //db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Esperando Activacion Internet");
                    //db2.consultaSinRetorno("ActualizaComandos");

                    Console.WriteLine("final activar,  en la olt: " + Ip);
                    return;
                }
                
                //fin SNMP Activar//

            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Si la ONU ya está registrada, solo asignamos la velocidad correspondiente
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void InternetActivarSoloVelocidad(string MAC, int Contrato, long Consecutivo, string VelocidadSubida, string VelocidadBajada, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            try
            {
                
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA WhiteList
                {
                    Console.WriteLine("No se encontro en la whitelist, en la olt: " + IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la whitelist");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    
                    Console.WriteLine("Cambiando Ancho de Banda");
                    CambiarAnchoBanda(BuscarMacBW(MAC, IPOLT_ONU), new Integer32(VelocidadSubida), new Integer32(VelocidadBajada), IPOLT_ONU);
                    Console.WriteLine("Guardando...");
                    SystemSave(new Integer32(4), IPOLT_ONU);

                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");
                    Console.WriteLine("Termino InternetActivarSoloVelocidad");
                    return;
                }

                //fin SNMP Activar//
               
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// DEshabilita internet
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void InternetSuspender(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA Whitelist
                {
                    Console.WriteLine("No se encontro la mac en la whitelist en OLT " + IPOLT_ONU);
                    //no se encuentra la mac en esta Whitelist
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro la mac en la whitelist");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    Console.WriteLine("SI se encontro la mac en la whitelist en OLT " + IPOLT_ONU);
                    Console.WriteLine("Borrando de la whitelist");
                    RemoveMacToWhiteList(BuscarIdWhiteList(MAC,IPOLT_ONU ),BuscarIdWhiteListBorrar(MAC,IPOLT_ONU),IPOLT_ONU);
                    Console.WriteLine("Guardando...");
                    SystemSave(new Integer32(4), IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");
                    Console.WriteLine("Termino InternetSuspender");
                    return;

                }
                //fin SNMP Activar//

               
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// DEshabilita internet
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void InternetReactivar(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdIlegales(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    //no se encuentra la mac en esta lista de ilegales
                    Console.WriteLine("No se encontro en la lista de ilegales en OLT " + IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la lista de ilegales");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    Console.WriteLine("SI se encontro en la lista de ilegales en OLT " + IPOLT_ONU);
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, IPOLT_ONU), ContarMacWhiteList(BuscarIdIlegales(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU);
                    
                    Console.WriteLine("Guradando...");
                    SystemSave(new Integer32(4), IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");
                    Console.WriteLine("termino InternetReactivar");
                    return;
                }

            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Cambio de velocidad para internet
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void InternetCambioPaquete(string MAC, int Contrato, long Consecutivo, string VelocidadSubida, string VelocidadBajada, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA WhiteList
                {
                    Console.WriteLine("No se encontro en la whitelist, en OLT " + IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la whitelist");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    Console.WriteLine("Si se encontro en la whitelist, en OLT " + IPOLT_ONU);
                    
                    Console.WriteLine("Cambiando ancho de banda");
                    CambiarAnchoBanda(BuscarMacBW(MAC, IPOLT_ONU), new Integer32(VelocidadSubida), new Integer32(VelocidadBajada), IPOLT_ONU);
                    Console.WriteLine("Guardando...");
                    SystemSave(new Integer32(4), IPOLT_ONU);

                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");
                    Console.WriteLine("termino cambio paquete, en OLT " + IPOLT_ONU);
                    return;
                }

                //fin SNMP Activar//
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Accede por Telnet a la OLT correspondiente, obtiene el listado de las ONU ilegales, las compara con la MAC
        /// solicitada, de haber coincidencia se obtiene el status y posición de la ONU en la OLT
        /// </summary>
        /// <param name="Ip"></param>
        /// <param name="User_"></param>
        /// <param name="Pass"></param>
        /// <param name="MAC"></param>
        private void TVActivar(string Ip, string User_, string Pass, string MAC, string Contrato, long Consecutivo)
        {
            try
            {
                
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdIlegales(MAC, Ip).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    Console.WriteLine("no se encontro en ilegales, olt: " + Ip);
                    Console.WriteLine("No se encontro en la lista de ilegales en la olt: " + Ip);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la lista de ilegales en la olt: " + Ip);
                    db2.consultaSinRetorno("ActualizaComandos");
                    //no se encuentra la mac en esta lista de ilegales
                    return;
                }
                else
                {
                    Console.WriteLine("se encontro en la lista de ilegales, en la olt: " + Ip);
                    Console.WriteLine("Agregando mac a la whitelist");
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, Ip), ContarMacWhiteList(BuscarIdIlegales(MAC, Ip), Ip), Ip);

                    System.Threading.Thread.Sleep(120000);

                    string macadresstelnet = Regex.Replace(MAC.Replace(" ", ""), "(.{2})(?!$)", "$0-").ToLower();
                    String[] position = sendCommand(listaConexiones[Ip], "show onu-position " + macadresstelnet);
                    foreach (String pos in position)
                    {
                        if (pos.Contains("Found ONU"))
                        {
                            String[] posSep = { "(", ")" };
                            String[] posAux = pos.Split(posSep, StringSplitOptions.RemoveEmptyEntries);
                            Console.WriteLine(posAux[1]);

                            String[] portPosSep = { "-" };
                            String[] portPos = posAux[1].Split(portPosSep, StringSplitOptions.RemoveEmptyEntries);
                            string puerto = portPos[1];
                            string posicion = portPos[2];

                            if (posAux[2].Contains("online"))
                            {
                                /// Aqui va el codigo cuando está online
                                String[] sep3 = { "-", ")" };
                                String[] NumeroONUAux = pos.Split(sep3, StringSplitOptions.RemoveEmptyEntries);

                                sendCommand(listaConexiones[Ip], "olt " + NumeroONUAux[1]);
                                sendCommand(listaConexiones[Ip], "onu " + NumeroONUAux[2]);
                                sendCommand(listaConexiones[Ip], "catv enable");
                                sendCommand(listaConexiones[Ip], "exit");
                                sendCommand(listaConexiones[Ip], "exit");
                                sendCommand(listaConexiones[Ip], "system save all");

                                DBFuncion db2 = new DBFuncion();
                                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                                db2.consultaSinRetorno("ActualizaComandos");

                                DBFuncion db1 = new DBFuncion();
                                db1.agregarParametro("@Contrato", System.Data.SqlDbType.BigInt, Contrato);
                                db1.agregarParametro("@olt", System.Data.SqlDbType.VarChar, 1);
                                db1.agregarParametro("@onu", System.Data.SqlDbType.Int, 1);
                                db1.agregarParametro("@Vlan", System.Data.SqlDbType.Int, Global.VLan);
                                db1.agregarParametro("@IpOlt", System.Data.SqlDbType.VarChar, Ip);
                                db1.agregarParametro("@Consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                                db1.agregarParametro("@Whitelist", System.Data.SqlDbType.Bit, 1);
                                db1.consultaSinRetorno("Actualiza_PosicionOnu");

                                return;
                            }
                            else
                            {
                                RemoveMacToWhiteList(BuscarIdWhiteList(MAC, Ip), BuscarIdWhiteListBorrar(MAC, Ip), Ip);
                            }
                        }
                    }

                    //DBFuncion db2 = new DBFuncion();
                    //db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 0);
                    //db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    //db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Esperando Activacion TV");
                    //db2.consultaSinRetorno("ActualizaComandos");


                    Console.WriteLine("fin activarTv, en la olt: " + Ip);
                    return;

                }

                //fin SNMP Activar//

            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Si la ONU ya está registrada, solo habilitamos la TV
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void TVActivarSoloHabilita(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            try
            {
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA WhiteList
                {
                    Console.WriteLine("No se encontro en la whitelist, en OLT " + IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la whitelist");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    Console.WriteLine("Si se encontro en la whitelist, en OLT " + IPOLT_ONU);

                    if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("2"))//2= offline
                    {
                        Console.WriteLine("ONU offline en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU offline en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("0") || BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals(""))
                    {
                        Console.WriteLine("ONU Apagada en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU Apagada en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else
                    {
                        Console.WriteLine("ONU online en OLT " + IPOLT_ONU);
                        Console.WriteLine("Cambiando estado CATV");
                        CambiarEstatusCATV(indexOnlineCATV(MAC, IPOLT_ONU), new Integer32(1), IPOLT_ONU);// 1 = activar, 2= desactivar
                        Console.WriteLine("Guardando...");
                        SystemSave(new Integer32(4), IPOLT_ONU);

                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                        db2.consultaSinRetorno("ActualizaComandos");
                        Console.WriteLine("termino TVActivarSoloHabilita ,en OLT " + IPOLT_ONU);
                        return;
                    }

                }

            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Solo deshabilita TV
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void TVSuspender(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                if (BuscarIdIlegales(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    Console.WriteLine("No esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    //snmp activa cuando no tiene asignada OLT//
                    if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA Whitelist
                    {
                        Console.WriteLine("No esta en la Whitelist ,en OLT " + IPOLT_ONU);
                        //no se encuentra la mac en esta Whitelist
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la olt " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else
                    {
                        Console.WriteLine("Si esta en la Whitelist ,en OLT " + IPOLT_ONU);

                        if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("2"))//2= offline
                        {
                            Console.WriteLine("ONU offline en OLT " + IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU offline en OLT " + IPOLT_ONU);
                            db2.consultaSinRetorno("ActualizaComandos");
                        }
                        else if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("0") || BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals(""))
                        {
                            Console.WriteLine("ONU Apagada en OLT " + IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU Apagada en OLT " + IPOLT_ONU);
                            db2.consultaSinRetorno("ActualizaComandos");
                        }
                        else
                        {
                            Console.WriteLine("ONU online en OLT " + IPOLT_ONU);
                            Console.WriteLine("Cambiando estado CATV");
                            CambiarEstatusCATV(indexOnlineCATV(MAC, IPOLT_ONU), new Integer32(2), IPOLT_ONU);// 1 = activar, 2= desactivar
                            Console.WriteLine("Guardando...");
                            SystemSave(new Integer32(4), IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                            db2.consultaSinRetorno("ActualizaComandos");
                            Console.WriteLine("Termino TvSuspender en OLT " + IPOLT_ONU);
                            
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Si esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    Console.WriteLine("Agregando la mac a la whitelist,en OLT " + IPOLT_ONU);
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, IPOLT_ONU), ContarMacWhiteList(BuscarIdIlegales(MAC, IPOLT_ONU), IPOLT_ONU) + 1, IPOLT_ONU);

                    if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("2"))//2= offline
                    {
                        Console.WriteLine("ONU offline en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU offline en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("0") || BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals(""))
                    {
                        Console.WriteLine("ONU Apagada en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU Apagada en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else
                    {
                        Console.WriteLine("ONU online en OLT " + IPOLT_ONU);
                        Console.WriteLine("Cambiando status CATV ,en OLT " + IPOLT_ONU);
                        CambiarEstatusCATV(indexOnlineCATV(MAC, IPOLT_ONU), new Integer32(2), IPOLT_ONU);// 1 = activar, 2= desactivar
                        Console.WriteLine("Quitando mac de la whitelist,en OLT " + IPOLT_ONU);
                        RemoveMacToWhiteList(BuscarIdWhiteList(MAC, IPOLT_ONU), BuscarIdWhiteListBorrar(MAC, IPOLT_ONU), IPOLT_ONU);
                        Console.WriteLine("GUARDANDO...");
                        SystemSave(new Integer32(4), IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                        db2.consultaSinRetorno("ActualizaComandos");
                        Console.WriteLine("Termino TvSuspender, en OLT " + IPOLT_ONU);
                    }

                }
                //fin SNMP Activar//
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Si la ONU ya está registrada, solo habilitamos la TV
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void TVReactivar(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdIlegales(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    Console.WriteLine("No esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA Whitelist
                    {
                        Console.WriteLine("No esta en la whitelist ,en OLT " + IPOLT_ONU);
                        //no se encuentra la mac en esta Whitelist
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la olt " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }else{
                        Console.WriteLine("Si esta en la whitelist ,en OLT " + IPOLT_ONU);

                        if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("2"))//2= offline
                        {
                            Console.WriteLine("ONU offline en OLT " + IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU offline en OLT " + IPOLT_ONU);
                            db2.consultaSinRetorno("ActualizaComandos");
                        }
                        else if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("0") || BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals(""))
                        {
                            Console.WriteLine("ONU Apagada en OLT " + IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU Apagada en OLT " + IPOLT_ONU);
                            db2.consultaSinRetorno("ActualizaComandos");
                        }
                        else
                        {
                            Console.WriteLine("ONU online en OLT " + IPOLT_ONU);
                            Console.WriteLine("Cambiando estatus CATV");
                            CambiarEstatusCATV(indexOnlineCATV(MAC, IPOLT_ONU), new Integer32(1), IPOLT_ONU);// 1 = activar, 2= desactivar
                            Console.WriteLine("Guardando...");
                            SystemSave(new Integer32(4), IPOLT_ONU);
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                            db2.consultaSinRetorno("ActualizaComandos");
                            Console.WriteLine("Termino ReactivarTV ,en OLT " + IPOLT_ONU);
                            return;
                        }
                    }
                    
                
                }
                else
                {
                    Console.WriteLine("Si esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    Console.WriteLine("Agregando mac a la whitelist");
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, IPOLT_ONU), ContarMacWhiteList(BuscarIdIlegales(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU);

                    if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("2"))//2= offline
                    {
                        Console.WriteLine("ONU offline en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU offline en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else if (BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals("0") || BuscarStatusOnline(indexOnline(MAC, BuscarIdWhiteList(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU).Equals(""))
                    {
                        Console.WriteLine("ONU Apagada en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 12);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "ONU Apagada en OLT " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else
                    {
                        Console.WriteLine("ONU online en OLT " + IPOLT_ONU);
                        Console.WriteLine("Cambiando estado CATV");
                        CambiarEstatusCATV(indexOnlineCATV(MAC, IPOLT_ONU), new Integer32(1), IPOLT_ONU);// 1 = activar, 2= desactivar
                        Console.WriteLine("Quitando mac de la whitelist");
                        RemoveMacToWhiteList(BuscarIdWhiteList(MAC, IPOLT_ONU), BuscarIdWhiteListBorrar(MAC, IPOLT_ONU), IPOLT_ONU);
                        Console.WriteLine("Guardando...");
                        SystemSave(new Integer32(4), IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                        db2.consultaSinRetorno("ActualizaComandos");
                        Console.WriteLine("Termino ReactivarTV ,en OLT " + IPOLT_ONU);
                        return;
                    }

                }

                //fin reactivar snmp

            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Elimina mac de la olt donde se registro
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void BorrarTodo(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                //snmp activa cuando no tiene asignada OLT//
                if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA Whitelist
                {
                    Console.WriteLine("No se encontro la mac en la whitelist en OLT " + IPOLT_ONU);
                    //no se encuentra la mac en esta Whitelist
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro la mac en la whitelist");
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                else
                {
                    Console.WriteLine("Si se encontro la mac en la whitelist en OLT " + IPOLT_ONU);
                    Console.WriteLine("Borrando mac de la whitelist");
                    RemoveMacToWhiteList(BuscarIdWhiteList(MAC, IPOLT_ONU), BuscarIdWhiteListBorrar(MAC, IPOLT_ONU), IPOLT_ONU);
                    Console.WriteLine("Guardando...");
                    SystemSave(new Integer32(4), IPOLT_ONU);
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");

                    DBFuncion db1 = new DBFuncion();
                    db1.agregarParametro("@Contrato", System.Data.SqlDbType.BigInt, Contrato);
                    db1.agregarParametro("@olt", System.Data.SqlDbType.VarChar, 0);
                    db1.agregarParametro("@onu", System.Data.SqlDbType.Int, 0);
                    db1.agregarParametro("@Vlan", System.Data.SqlDbType.Int, 0);
                    db1.agregarParametro("@IpOlt", System.Data.SqlDbType.VarChar, "");
                    db1.agregarParametro("@Consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db1.consultaSinRetorno("Actualiza_PosicionOnu");
                    Console.WriteLine("Fin Borrar todo, en OLT " + IPOLT_ONU);
                    return;

                }
                //fin SNMP Activar//
                
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Envía comandos para resetar la onu
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void Reset(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                OLT OLT_ONU = new OLT();
                OLT_ONU = listaOLT.Where(t => t.Ip == IPOLT_ONU).FirstOrDefault();
                //TelnetConnection tc = new TelnetConnection(OLT_ONU.Ip, 23);
                TelnetConnection tc = new TelnetConnection(OLT_ONU.Ip, 9878);
                string s = tc.Login(OLT_ONU.User_, OLT_ONU.Pass, 3000);

                if (getConnectionSuccess(s))
                {
                    //Console.WriteLine("Connected...");
                    //Console.WriteLine(s);

                    sendCommand(tc, "olt " + NumeroOLT.ToString());
                    sendCommand(tc, "onu " + NumeroONU.ToString());
                    sendCommand(tc, "ctc reboot");
                    sendCommand(tc, "exit");
                    sendCommand(tc, "exit");
                    sendCommand(tc, "system save all");

                    tc.CloseConnection();

                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                    db2.consultaSinRetorno("ActualizaComandos");

                    DBFuncion db1 = new DBFuncion();
                    db1.agregarParametro("@Contrato", System.Data.SqlDbType.BigInt, Contrato);
                    db1.agregarParametro("@olt", System.Data.SqlDbType.VarChar, 0);
                    db1.agregarParametro("@onu", System.Data.SqlDbType.Int, 0);
                    db1.agregarParametro("@Vlan", System.Data.SqlDbType.Int, 0);
                    db1.agregarParametro("@IpOlt", System.Data.SqlDbType.VarChar, "");
                    db1.agregarParametro("@Consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db1.consultaSinRetorno("Actualiza_PosicionOnu");
                }
                else
                {
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Sin conexión OLT " + OLT_ONU.Ip);
                    db2.consultaSinRetorno("ActualizaComandos");
                }
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        /// <summary>
        /// Cambio de paquete de tv no se procesa, solo lo ponemos como ejecutado
        /// </summary>
        /// <param name="MAC"></param>
        /// <param name="Contrato"></param>
        /// <param name="Consecutivo"></param>
        /// <param name="VelocidadSubida"></param>
        /// <param name="VelocidadBajada"></param>
        /// <param name="NumeroOLT"></param>
        /// <param name="NumeroONU"></param>
        /// <param name="IPOLT_ONU"></param>
        private void TVCambioPaquete(long Consecutivo, string MAC)
        {
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
           
            DBFuncion db2 = new DBFuncion();
            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Cambio de paquete de TV no procesado");
            db2.consultaSinRetorno("ActualizaComandos");
            if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
            {
                comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
            }
            Console.WriteLine("Termino TVCambioPaquete ");
            
        }

        
        /// <summary>
        /// Verifica que se obtenga el prompt correspondiente si la conexión por Telnet fue exitosa
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool getConnectionSuccess(string s)
        {
            string prompt = s.TrimEnd();
            prompt = s.Substring(prompt.Length - 1, 1);
            if (prompt != "$" && prompt != ">" && prompt != "#")
                return false;
            return true;
        }

        /// <summary>
        /// Envía el comando correspondiente por Telnet y obtiene la salida del comando línea por línea
        /// </summary>
        /// <param name="tc"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public String[] sendCommand(TelnetConnection tc, string command)
        {
            if (tc.IsConnected)
            {
                tc.WriteLine(command);
                System.Threading.Thread.Sleep(3000);
                string prompt = tc.Read();

                Console.WriteLine(command);
                Console.WriteLine(prompt);

                if (prompt.Contains("Press any key to continue"))
                {
                    string promptAux = "";
                    do
                    {
                        promptAux = "";
                        tc.WriteLine("S");
                        System.Threading.Thread.Sleep(5000);

                        promptAux = tc.Read();
                        prompt = prompt + promptAux;
                    } while (promptAux.Contains("Press any key to continue"));
                }

                String[] strlist = prompt.Split('\n');

                return strlist;
            }
            else
            {
                String[] aux = { "Disconnected..." };
                return aux;
            }
        }




        ///SNMMP CHRISTIAN ///
        ///una mac tiene mas de un indice asociado para mas de una cosa, es por lo que se realizan diferentes buquedas y se obtienen diferentes indices
        const String onlinestauts = ".1.3.6.1.4.1.34592.1.3.4.1.1.11";//Devuelve si las mac en la whitelist estan online o offline
        const String macCATV = ".1.3.6.1.4.1.34592.1.3.4.1.1.7.1";//Devulve la lista de mac que se encuentran en la whitelist para obtener su indice y asi ver su status online o offline
        const String CATVTable = ".1.3.6.1.4.1.34592.1.3.4";//devuelve la tabla donde se puede obtener configuracione de la onu, sirve para obtener indices en las busquedas
        const String ilegalesTable = ".1.3.6.1.4.1.34592.1.3.1.6.4";//OID para ver la tabla que contiene la lista de ilegales
        const String ListaIlegales = ".1.3.6.1.4.1.34592.1.3.1.6.4.1.2";//OID para concer la lista de las illegales
        const String Whitelist = ".1.3.6.1.4.1.34592.1.3.1.6.7.1.2";//OID para revisar la whitelist
        const String AddWhitelist = ".1.3.6.1.4.1.34592.1.3.1.6.7";//OID es usado para agregar o eliminar una MAC a la whitelist
        const String OnuIdDadasDeAlta = ".1.3.6.1.4.1.34592.1.3.3.12.1.1.3";//OID para conocer todos los ONU ID que tenemos dados de alta y están activos
        const String MaxAddresActivas = ".1.3.6.1.4.1.34592.1.3.3.12.1.1.4";//OID para conocer las MAC ADDRESS de las ONUS que están activas
        const String StatusOnus = ".1.3.6.1.4.1.34592.1.3.3.12.1.1.5";//OID para concer el estatus de las ONUs conectadas
        const String AnchoDeBanda = ".1.3.6.1.4.1.34592.1.3.3.12.3.1.1.2";//OID para concer el maximo ancho de bnanda permitido
        const String OnusActivasPorPuerto = ".1.3.6.1.4.1.34592.1.3.3.1.1.6";//OID para concer las onus activas en cada puerto
        const String StatusCATV = ".1.3.6.1.4.1.34592.1.3.4.2.2.1.4";//OID para conocer el estatus del CATV
        const String OperarSystem = ".1.3.6.1.4.1.34592.1.3.1.2.12.0";//OID para operar como System para guardar (Integer, 4 para guardar)
        
        
        private void get_Operation(String oid, String ip)
        {
            OctetString community = new OctetString("public");
            AgentParameters param = new AgentParameters(community);//community permisos de lectura-escritura --- public(read) -- private(read and write)
            // Set SNMP version to 1 (or 2)
            param.Version = SnmpVersion.Ver2;
            IpAddress agent = new IpAddress(ip);
            // Construct target
            UdpTarget target = new UdpTarget((IPAddress)agent, 9161, 2000, 1);//peer,port,timeout,retry
            // Pdu class used for all requests
            Pdu pdu = new Pdu(PduType.Get);
            pdu.VbList.Add(oid); //OID para revisar la whitelist
            SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);//pdu=get,param=public
            // If result is null then agent didn't reply or we couldn't parse the reply.
            if (result != null)
            {
                if (result.Pdu.ErrorStatus != 0)
                {
                    // agent reported an error with the request
                    //textBox1.AppendText("Error in SNMP reply");
                }
                else
                {
                    // Reply variables are returned in the same order as they were added
                    //  to the VbList
                    //textBox1.AppendText(result.Pdu.VbList[0].Oid.ToString() + " (" + SnmpConstants.GetTypeName(result.Pdu.VbList[0].Value.Type)
                    //   + "): " + result.Pdu.VbList[0].Value.ToString() + "\r\n");
                }
            }
            else
            {
                //textBox1.AppendText("No response received from SNMP agent.");
            }
            target.Close();
        }//operacion get, requiere definir el subindice al que se quiere acceder ej: get_Operation(ListaIlegales+".1.1");

        private void getSubtree_Operation(string oid, String ip)
        {
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(oid);
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return;
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return;
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return;
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            //textBox1.AppendText(kvp.Value[column].ToString() + "\r\n");
                        }

                    }
                }
            }
        }//operation get subtree

        public static string InstanceToString(uint[] instance)//metodo necesario para getSubtree_Operation
        {
            StringBuilder str = new StringBuilder();
            foreach (uint v in instance)
            {
                if (str.Length == 0)
                    str.Append(v);
                else
                    str.AppendFormat(".{0}", v);
            }
            return str.ToString();
        }

        private void AddMacToWhiteList(byte[] StringMac, String WhiteList, String MacEntryId, String ip)//mac,WhiteList (va de 1 a 8 que tiene disponibles la OLT) ,authWhitelistMacEntryID
        {
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 500, 1);//ip conexion, puerto, timeout,retry
            Pdu pdu = new Pdu(PduType.Set);//se define la operacion set para SNMP
            //los parametros que recibe la operacion de set, son en el siguiente orden: la oid para agregar una mac a la whitelist,
            //.1(la tabla que almacena la info), .3(la columna de rowstatus de la tabla), .2(la columna con la mac en la tabla)
            //WhiteList(el numero de la whitelist en la que se registra), MacEntryId(el numero de la siguiente fila)
            pdu.VbList.Add(new Oid(AddWhitelist + ".1.3." + WhiteList + "." + MacEntryId), new Integer32(4));//OID PARA EL ROWSTATUS -- (4)CREATE AND GO
            pdu.VbList.Add(new Oid(AddWhitelist + ".1.2." + WhiteList + "." + MacEntryId), new OctetString(StringMac));//OID PARA AGREGAR LA MAC

            AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("private"));//private permiso de lectura y escritura
            SnmpV2Packet response;
            try
            {
                // Send request and wait for response
                response = target.Request(pdu, aparam) as SnmpV2Packet;
            }
            catch (Exception ex)
            {
                // If exception happens, it will be returned here
                //textBox1.AppendText("Request failed with exception: " + ex.Message);
                target.Close();
                return;
            }
            // Make sure we received a response
            if (response == null)
            {
                //textBox1.AppendText("Error in sending SNMP request.");
            }
            else
            {
                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned ErrorStatus " + response.Pdu.ErrorStatus + " on index " + response.Pdu.ErrorIndex);
                }
                else
                {
                    // Everything is ok. Agent will return the new value for the OID we changed
                    //textBox1.AppendText("Agent response " + response.Pdu[0].Oid.ToString() + " : " + response.Pdu[0].Value.ToString() + "\r\n");
                }
                //textBox1.AppendText("\r\n");
            }
        }

        private void RemoveMacToWhiteList(String WhiteList, String MacEntryId, String ip)//authWhitelistMacEntryID
        {
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 500, 1);//ip conexion, puerto, timeout,retry
            Pdu pdu = new Pdu(PduType.Set);//operacion set
            //como en el caso de la funcion AddMacToWhiteList, pero esta vez para eliminar solo se cambia la columna de rowstatus
            //debe recibir la whitelist  en la que esta y el numero de la fila que se desea quitar
            pdu.VbList.Add(new Oid(AddWhitelist + ".1.3." + WhiteList + "." + MacEntryId), new Integer32(6));//OID PARA EL ROWSTATUS -- (6)Destroy

            AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("private"));
            SnmpV2Packet response;
            try
            {
                // Send request and wait for response
                response = target.Request(pdu, aparam) as SnmpV2Packet;
            }
            catch (Exception ex)
            {
                // If exception happens, it will be returned here
                //textBox1.AppendText("Request failed with exception: " + ex.Message);
                target.Close();
                return;
            }
            // Make sure we received a response
            if (response == null)
            {
                //textBox1.AppendText("Error in sending SNMP request.");
            }
            else
            {
                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned ErrorStatus " + response.Pdu.ErrorStatus + " on index " + response.Pdu.ErrorIndex);
                }
                else
                {
                    // Everything is ok. Agent will return the new value for the OID we changed
                    //textBox1.AppendText("Agent response " + response.Pdu[0].Oid.ToString() + " : " + response.Pdu[0].Value.ToString() + "\r\n");
                }
                //textBox1.AppendText("\r\n");
            }
        }

        private void CambiarAnchoBanda(String MacIndex, Integer32 Subida, Integer32 Bajada, String ip)//BW(256...1000000)
        {
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 500, 1);//ip conexion, puerto, timeout,retry
            Pdu pdu = new Pdu(PduType.Set);
            //ancho de banda es la oid que contiene los valores de subida y vajada a cambiar
            // debe recibir el macIndex que es el inidce que relaciona la mac con estos valores para cambiar el corecto
            pdu.VbList.Add(new Oid(AnchoDeBanda + ".1." + MacIndex + "." + 1), Subida);//OID PARA Cambiar el ancho de banda BW - Integer32 (256...1000000)
            pdu.VbList.Add(new Oid(AnchoDeBanda + ".1." + MacIndex + "." + 2), Bajada);//OID PARA Cambiar el ancho de banda BW - Integer32 (256...1000000)

            AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("private"));
            SnmpV2Packet response;
            try
            {
                // Send request and wait for response
                response = target.Request(pdu, aparam) as SnmpV2Packet;
            }
            catch (Exception ex)
            {
                // If exception happens, it will be returned here
                //textBox1.AppendText("Request failed with exception: " + ex.Message);
                target.Close();
                return;
            }
            // Make sure we received a response
            if (response == null)
            {
                //textBox1.AppendText("Error in sending SNMP request.");
            }
            else
            {
                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned ErrorStatus " + response.Pdu.ErrorStatus + " on index " + response.Pdu.ErrorIndex);
                }
                else
                {
                    // Everything is ok. Agent will return the new value for the OID we changed
                    //textBox1.AppendText("Agent response " + response.Pdu[0].Oid.ToString() + " : " + response.Pdu[0].Value.ToString() + "\r\n");
                }
                //textBox1.AppendText("\r\n");
            }
        }

        private void CambiarEstatusCATV(String Id, Integer32 Satus, String ip)//Status eneble(1) - disable(2)
        {
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 500, 1);//ip conexion, puerto, timeout,retry
            Pdu pdu = new Pdu(PduType.Set);
            //StatusCATV es la oid con los status de CATV, id es el index que permite relacionarlo con la mac que queremos
            pdu.VbList.Add(new Oid(StatusCATV + "." + Id), Satus);//OID PARA Cambiar el ststus de CATV --- enable(1) - disable(2)
            AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("private"));
            SnmpV2Packet response;
            try
            {
                // Send request and wait for response
                response = target.Request(pdu, aparam) as SnmpV2Packet;
            }
            catch (Exception ex)
            {
                // If exception happens, it will be returned here
                //textBox1.AppendText("Request failed with exception: " + ex.Message);
                target.Close();
                return;
            }
            // Make sure we received a response
            if (response == null)
            {
                //textBox1.AppendText("Error in sending SNMP request.");
            }
            else
            {
                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned ErrorStatus " + response.Pdu.ErrorStatus + " on index " + response.Pdu.ErrorIndex);
                }
                else
                {
                    // Everything is ok. Agent will return the new value for the OID we changed
                    //textBox1.AppendText("Agent response " + response.Pdu[0].Oid.ToString() + " : " + response.Pdu[0].Value.ToString() + "\r\n");
                }
                //textBox1.AppendText("\r\n");
            }
        }

        private void SystemSave(Integer32 Satus, String ip)//Status reset(2) - default(3) - saveConfig(4) - restore(5) - delete(6)
        {
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 500, 1);//ip conexion, puerto, timeout,retry
            Pdu pdu = new Pdu(PduType.Set);
            pdu.VbList.Add(new Oid(OperarSystem), Satus);//OID PARA Operar como system para guardar --- reset(2) - default(3) - saveConfig(4) - restore(5) - delete(6)
            AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("private"));
            SnmpV2Packet response;
            try
            {
                // Send request and wait for response
                response = target.Request(pdu, aparam) as SnmpV2Packet;
            }
            catch (Exception ex)
            {
                // If exception happens, it will be returned here
                //textBox1.AppendText("Request failed with exception: " + ex.Message);
                target.Close();
                return;
            }
            // Make sure we received a response
            if (response == null)
            {
                //textBox1.AppendText("Error in sending SNMP request.");
            }
            else
            {
                // Check if we received an SNMP error from the agent
                if (response.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned ErrorStatus " + response.Pdu.ErrorStatus + " on index " + response.Pdu.ErrorIndex);
                }
                else
                {
                    // Everything is ok. Agent will return the new value for the OID we changed
                    //textBox1.AppendText("Agent response " + response.Pdu[0].Oid.ToString() + " : " + response.Pdu[0].Value.ToString() + "\r\n");
                }
                //textBox1.AppendText("\r\n");
            }
        }

        public void getTable(String oid, String ip)
        {
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(oid);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return;
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return;
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return;
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                foreach (uint column in tableColumns)
                {
                    //textBox1.AppendText("\t\tColumn id" + column);
                }
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            //textBox1.AppendText("\t" + kvp.Value[column].ToString() + "(" + SnmpConstants.GetTypeName(kvp.Value[column].Type) + ")");
                        }
                        else
                        {
                            //textBox1.AppendText("\t-");
                        }
                    }
                    //textBox1.AppendText("\r\n");
                }
            }
            //textBox1.AppendText("\r\n");
        }//obtiene la tabla de la oid

        private void BuscarMacWhiteList(String Mac, String ip)
        {
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(Whitelist);
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);//ip conexion, puerto, timeout,retry
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // revisa todos los registros que pertencen a la raiz que se manda en la oid
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return;
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return;
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return;
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                //textBox1.AppendText("OID Key: " + kvp.Key + " - " + kvp.Value[column].ToString() + "\r\n");
                            }
                        }

                    }
                }
            }
        }//operation get subtree para buscar una mac en la whitelist

        private void BuscarMacIlegales(String Mac, String ip)
        {
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(ListaIlegales);
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return;
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return;
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return;
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                //textBox1.AppendText("OID Key: " + kvp.Key + " - " + kvp.Value[column].ToString() + "\r\n");
                            }
                        }

                    }
                }
            }
        }//operation get subtree para buscar una mac en la lista de ilegales

        private String ContarMacWhiteList(String IndexList, String ip)
        {
            int count = 0;//contador para registrar los registros que hay en la whitelist
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            //se manda el parametro IndexList para que cuente solo los elementos que existen en la whitelist a la que queremos agregar la nueva mac
            Oid startOid = new Oid(Whitelist + "." + IndexList);
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            count = count + 1;//para cada registro obtenido en el subtree aumenta el contador

                        }

                    }
                }
            }
            count = count + 1;//agrega un uno al final para que sea el siguiente valor a agregar
            return count.ToString();
        }//operation get subtree para contar cuantos registros existen en la whitelist

        public String BuscarIdIlegales(String Mac, String ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(ilegalesTable);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }

                        if (Mac.Equals(result[strInst][column].ToString()))
                        {
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[14];
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                //foreach (uint column in tableColumns)
                //{
                //    textBox1.AppendText("\t\tColumn id" + column);
                //}
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                index = kvp.Key.Substring(0, 1);
                            }

                        }

                    }
                }
            }
            return index;
        }//obtiene la tabla de la oid para obtener el index, y saber en que whithlist se debe agregar

        public String BuscarIdWhiteList(String Mac, String Ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(Ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(AddWhitelist);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }

                        if (Mac.Equals(result[strInst][column].ToString()))
                        {
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[14];
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                //foreach (uint column in tableColumns)
                //{
                //    textBox1.AppendText("\t\tColumn id" + column);
                //}
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                index = kvp.Key.Substring(0, 1);
                            }
                        }

                    }
                }
            }
            return index;
        }//obtiene la tabla de la oid para obtener el index, y saber con que whitelist operar

        private String BuscarMacBW(String Mac, String Ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(MaxAddresActivas);
            IpAddress peer = new IpAddress(Ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }

                        if (Mac.Equals(result[strInst][column].ToString()))
                        {
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[15] + "." + partes[16];
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                //textBox1.AppendText("OID Key: " + kvp.Key + " - " + kvp.Value[column].ToString() + "\r\n");
                                index = kvp.Key;
                            }
                        }

                    }
                }
            }
            return index;
        }//operation get subtree para buscar una mac, y despues poder cambiar su velocidad

        public String BuscarMacCATV(String Mac, String ip)
        {
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(CATVTable);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                //foreach (uint column in tableColumns)
                //{
                //    textBox1.AppendText("\t\tColumn id" + column);
                //}
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                index = kvp.Key.Substring(2, 5);
                            }
                        }

                    }
                }
            }
            return index;
        }//obtiene la tabla de la oid para obtener el index, y saber con que whitelist operar

        public String BuscarMacOnline(String Mac, String ip)//obtiene el indice de la siguiente mac par ver si esta online o no
        {
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(CATVTable);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                //foreach (uint column in tableColumns)
                //{
                //    textBox1.AppendText("\t\tColumn id" + column);
                //}
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                index = kvp.Key.Substring(4, 3);
                            }
                        }

                    }
                }
            }
            return index;
        }//obtiene la tabla de la oid para obtener el index, y saber con que whitelist operar

        private String BuscarStatusOnline(String indexMac, String Ip)// obtiene el status de la onu para saber si esta activa o no a partir del indice mandado
        {
            string aux = "";
            String auxindex = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(onlinestauts);
            IpAddress peer = new IpAddress(Ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {

                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);

                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                            //Console.WriteLine("buscando ststus :" + result[strInst][column]);
                        }

                        auxindex = "";
                        aux = curOid.ToString();
                        string[] partes = aux.Split('.');
                        auxindex = partes[14] + "." + partes[15];
                        //textBox1.AppendText("index :" + index + "\r\n");
                        if (auxindex.Equals(indexMac))
                        {
                            index = result[strInst][column].ToString();
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Key.ToString().Equals(indexMac))
                            {
                                index = kvp.Value[column].ToString();
                                return index;
                            }
                        }

                    }
                }
            }
            return index;
        }//operation get subtree para buscar una mac, y despues poder cambiar su velocidad

        public String BuscarIdWhiteListBorrar(String Mac, String ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            // Prepare agent information
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            IpAddress peer = new IpAddress(ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            // This is the table OID supplied on the command line
            Oid startOid = new Oid(AddWhitelist);
            // Each table OID is followed by .1 for the entry OID. Add it to the table OID
            startOid.Add(1); // Add Entry OID to the end of the table OID
            // Prepare the request PDU
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                        }

                        if (Mac.Equals(result[strInst][column].ToString()))
                        {
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[15];
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }
                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText("No results returned.\r\n");
            }
            else
            {
                //textBox1.AppendText("Instance");
                //foreach (uint column in tableColumns)
                //{
                //    textBox1.AppendText("\t\tColumn id" + column);
                //}
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    //textBox1.AppendText(kvp.Key);
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            if (kvp.Value[column].ToString().Equals(Mac.ToUpper()))
                            {
                                string[] partes = kvp.Key.Split('.');
                                index = partes[1];
                                //index = kvp.Key.Substring(2, 1);
                            }
                        }

                    }
                }
            }
            return index;
        }//obtiene la tabla de la oid para obtener el index, y saber cual mac borrar

        private String indexOnline(string mac,string puerto ,string Ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(".1.3.6.1.4.1.34592.1.3.4.1.1.7.1." + puerto);
            IpAddress peer = new IpAddress(Ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    //textBox1.AppendText("curOid" + curOid + "\r\n");
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        //textBox1.AppendText("childOids!" + childOids.ToString() + "\r\n");
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        //textBox1.AppendText("instance" + instance.ToString() + "\r\n");
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        //textBox1.AppendText("strInst" + strInst + "\r\n");
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        //textBox1.AppendText("tableColumns.Contains(column):" + tableColumns.Contains(column) + "\r\n");
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                            //textBox1.AppendText("result[strInst][column]1:" + result[strInst][column].ToString() + "\r\n");
                            //Console.WriteLine("buscando indice :"+result[strInst][column]);
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                            //textBox1.AppendText("result[strInst][column]2:" + result[strInst][column].ToString() + "\r\n");
                        }

                        if (mac.Equals(result[strInst][column].ToString()))
                        {
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[14] + "." + partes[15];
                            return index;
                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }


                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText(curOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            //if (kvp.Value[column].ToString().Equals(mac.ToUpper()))
                            //{
                            //    //index = kvp.Key.Substring(4, 3);
                            //    //index = curOid.ToString();
                            //    index = column.ToString();
                            //    return index;

                            //}

                        }

                    }
                }
            }
            return index;
        }//operation get subtree

        private String indexOnlineCATV(string mac, string Ip)
        {
            String aux = "";
            String index = "";
            Dictionary<String, Dictionary<uint, AsnType>> result = new Dictionary<String, Dictionary<uint, AsnType>>();
            List<uint> tableColumns = new List<uint>();
            AgentParameters param = new AgentParameters(SnmpVersion.Ver2, new OctetString("public"));
            Oid startOid = new Oid(".1.3.6.1.4.1.34592.1.3.4.1.1.7.1");
            IpAddress peer = new IpAddress(Ip);
            UdpTarget target = new UdpTarget((IPAddress)peer, 9161, 2000, 1);
            //startOid.Add(1); // Add Entry OID to the end of the table OID
            Pdu bulkPdu = Pdu.GetNextPdu();
            bulkPdu.VbList.Add(startOid);
            // Current OID will keep track of the last retrieved OID and be used as 
            //  indication that we have reached end of table
            Oid curOid = (Oid)startOid.Clone();
            // Keep looping through results until end of table
            while (startOid.IsRootOf(curOid))
            {
                SnmpPacket res = null;
                try
                {
                    res = target.Request(bulkPdu, param);
                }
                catch (Exception ex)
                {
                    //textBox1.AppendText("Request failed: " + ex.Message);
                    //textBox1.AppendText("\r\n");
                    target.Close();
                    return "0";
                }
                // For GetBulk request response has to be version 2
                if (res.Version != SnmpVersion.Ver2)
                {
                    //textBox1.AppendText("Received wrong SNMP version response packet.");
                    target.Close();
                    return "0";
                }
                // Check if there is an agent error returned in the reply
                if (res.Pdu.ErrorStatus != 0)
                {
                    //textBox1.AppendText("SNMP agent returned error" + res.Pdu.ErrorStatus + "for request Vb index" + res.Pdu.ErrorIndex);
                    target.Close();
                    return "0";
                }
                // Go through the VbList and check all replies
                foreach (Vb v in res.Pdu.VbList)
                {
                    curOid = (Oid)v.Oid.Clone();
                    //textBox1.AppendText("curOid" + curOid + "\r\n");
                    // VbList could contain items that are past the end of the requested table.
                    // Make sure we are dealing with an OID that is part of the table
                    if (startOid.IsRootOf(v.Oid))
                    {
                        // Get child Id's from the OID (past the table.entry sequence)
                        uint[] childOids = Oid.GetChildIdentifiers(startOid, v.Oid);
                        //textBox1.AppendText("childOids!" + childOids.ToString() + "\r\n");
                        // Get the value instance and converted it to a dotted decimal
                        //  string to use as key in result dictionary
                        uint[] instance = new uint[childOids.Length - 1];
                        //textBox1.AppendText("instance" + instance.ToString() + "\r\n");
                        Array.Copy(childOids, 1, instance, 0, childOids.Length - 1);
                        String strInst = InstanceToString(instance);
                        //textBox1.AppendText("strInst" + strInst + "\r\n");
                        // Column id is the first value past <table oid>.entry in the response OID
                        uint column = childOids[0];
                        if (!tableColumns.Contains(column))
                            tableColumns.Add(column);
                        //textBox1.AppendText("tableColumns.Contains(column):" + tableColumns.Contains(column) + "\r\n");
                        if (result.ContainsKey(strInst))
                        {
                            result[strInst][column] = (AsnType)v.Value.Clone();
                            //textBox1.AppendText("result[strInst][column]1:" + result[strInst][column].ToString() + "\r\n");
                        }
                        else
                        {
                            result[strInst] = new Dictionary<uint, AsnType>();
                            result[strInst][column] = (AsnType)v.Value.Clone();
                            //textBox1.AppendText("result[strInst][column]2:" + result[strInst][column].ToString() + "\r\n");
                        }

                        if (mac.Equals(result[strInst][column].ToString()))
                        {
                            //textBox1.AppendText("childOids:" + childOids.ToString().Substring(31,3) + "\r\n");
                            //textBox1.AppendText("curOid:" + curOid.ToString().Substring(32, 3) + "\r\n");
                            //textBox1.AppendText("MAC :" + result[strInst][column].ToString() + "\r\n");
                            //index = curOid.ToString().Substring(32, 3);
                            index = "";
                            aux = curOid.ToString();
                            string[] partes = aux.Split('.');
                            index = partes[13] + "." + partes[14] + "." + partes[15];
                            return index;
                            //textBox1.AppendText("tamaño :" + partes.Length.ToString() + "\r\n");
                            //for (int i = 14; i < partes.Length; i++)
                            //{
                            //    //textBox1.AppendText("aux :" + partes[i].ToString() + "\r\n");
                            //    index = index + partes[i].ToString();
                            //}

                            //textBox1.AppendText("index :" + index + "\r\n");

                        }
                    }
                    else
                    {
                        // We've reached the end of the table. No point continuing the loop
                        break;
                    }


                }
                // If last received OID is within the table, build next request
                if (startOid.IsRootOf(curOid))
                {
                    bulkPdu.VbList.Clear();
                    bulkPdu.VbList.Add(curOid);
                }
            }
            target.Close();
            return index;
            if (result.Count <= 0)
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText("No results returned.");
                //textBox1.AppendText("\r\n");
            }
            else
            {
                //textBox1.AppendText(startOid.ToString());
                //textBox1.AppendText("\r\n");
                //textBox1.AppendText(curOid.ToString());
                //textBox1.AppendText("\r\n");
                foreach (KeyValuePair<string, Dictionary<uint, AsnType>> kvp in result)
                {
                    if (kvp.Key != "")
                    {
                        //textBox1.AppendText("OID Key: " + kvp.Key + " - ");
                    }
                    foreach (uint column in tableColumns)
                    {
                        if (kvp.Value.ContainsKey(column))
                        {
                            //if (kvp.Value[column].ToString().Equals(mac.ToUpper()))
                            //{
                            //    //index = kvp.Key.Substring(4, 3);
                            //    //index = curOid.ToString();
                            //    index = column.ToString();
                            //    return index;

                            //}

                        }

                    }
                }
            }
            return index;
        }//operation get subtree

        private void InternetActivarconIPTelnet(string MAC, int Contrato, long Consecutivo, string VelocidadSubida, string VelocidadBajada, int NumeroOLT, int NumeroONU, string IPOLT_ONU)
        {
            try
            {
                //string NumeroOLT = "";
                string macadresstelnet = Regex.Replace(MAC.Replace(" ", ""), "(.{2})(?!$)", "$0-").ToLower();

                String[] position = sendCommand(listaConexiones[IPOLT_ONU], "show onu-position " + macadresstelnet);
                foreach (String pos in position)
                {
                    if (pos.Contains("Found ONU"))
                    {
                        String[] posSep = { "(", ")" };
                        String[] posAux = pos.Split(posSep, StringSplitOptions.RemoveEmptyEntries);
                        Console.WriteLine(posAux[1]);

                        String[] portPosSep = { "-" };
                        String[] portPos = posAux[1].Split(portPosSep, StringSplitOptions.RemoveEmptyEntries);
                        string puerto = portPos[1];
                        string posicion = portPos[2];

                        if (posAux[2].Contains("online"))
                        {
                            /// Aqui va el codigo cuando está online
                            String[] sep3 = { "-", ")" };
                            String[] NumeroONUAux = pos.Split(sep3, StringSplitOptions.RemoveEmptyEntries);

                            sendCommand(listaConexiones[IPOLT_ONU], "olt " + NumeroONUAux[1]);
                            sendCommand(listaConexiones[IPOLT_ONU], "onu " + NumeroONUAux[2]);
                            //sendCommand(listaConexiones[IPOLT_ONU], "catv disable");
                            sendCommand(listaConexiones[IPOLT_ONU], "link 1");
                            sendCommand(listaConexiones[IPOLT_ONU], "sla upstream 0 1 " + VelocidadSubida + " 1");
                            sendCommand(listaConexiones[IPOLT_ONU], "sla downstream " + VelocidadBajada + " 512 1");
                            sendCommand(listaConexiones[IPOLT_ONU], "exit");
                            sendCommand(listaConexiones[IPOLT_ONU], "save");
                            sendCommand(listaConexiones[IPOLT_ONU], "exit");
                            sendCommand(listaConexiones[IPOLT_ONU], "exit");
                            sendCommand(listaConexiones[IPOLT_ONU], "system save all");

                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                            db2.consultaSinRetorno("ActualizaComandos");
                            return;
                        }
                     }
                  }
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }


        private void TVActivarSoloHabilitaConIPTelnet(string MAC, int Contrato, long Consecutivo, int NumeroOLT, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            try
            {

                //string NumeroOLT = "";
                string macadresstelnet = Regex.Replace(MAC.Replace(" ", ""), "(.{2})(?!$)", "$0-").ToLower();
                String[] position = sendCommand(listaConexiones[IPOLT_ONU], "show onu-position " + macadresstelnet);
                foreach (String pos in position)
                {
                    if (pos.Contains("Found ONU"))
                    {
                        String[] posSep = { "(", ")" };
                        String[] posAux = pos.Split(posSep, StringSplitOptions.RemoveEmptyEntries);
                        Console.WriteLine(posAux[1]);

                        String[] portPosSep = { "-" };
                        String[] portPos = posAux[1].Split(portPosSep, StringSplitOptions.RemoveEmptyEntries);
                        string puerto = portPos[1];
                        string posicion = portPos[2];

                        if (posAux[2].Contains("online"))
                        {
                            /// Aqui va el codigo cuando está online
                            String[] sep3 = { "-", ")" };
                            String[] NumeroONUAux = pos.Split(sep3, StringSplitOptions.RemoveEmptyEntries);

                            sendCommand(listaConexiones[IPOLT_ONU], "olt " + NumeroONUAux[1]);
                            sendCommand(listaConexiones[IPOLT_ONU], "onu " + NumeroONUAux[2]);
                            sendCommand(listaConexiones[IPOLT_ONU], "catv enable");
                            sendCommand(listaConexiones[IPOLT_ONU], "exit");
                            sendCommand(listaConexiones[IPOLT_ONU], "exit");
                            sendCommand(listaConexiones[IPOLT_ONU], "system save all");

                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                            db2.consultaSinRetorno("ActualizaComandos");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }

        private void TVReactivarTelnet(string MAC, int Contrato, long Consecutivo, int NumeroOLTold, int NumeroONU, string IPOLT_ONU, bool Whitelist)
        {
            
            Console.WriteLine("Consecutivo " + Consecutivo.ToString() + " MAC " + MAC);
            try
            {
                if (BuscarIdIlegales(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA LISTA DE ILEGALES
                {
                    Console.WriteLine("No esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    if (BuscarIdWhiteList(MAC, IPOLT_ONU).Equals(""))//BUSCA LA MAC EN LA Whitelist
                    {
                        Console.WriteLine("No esta en la whitelist ,en OLT " + IPOLT_ONU);
                        DBFuncion db2 = new DBFuncion();
                        db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 5);
                        db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                        db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se encontro en la olt " + IPOLT_ONU);
                        db2.consultaSinRetorno("ActualizaComandos");
                    }
                    else
                    {
                        Console.WriteLine("Si esta en la whitelist ,en OLT " + IPOLT_ONU);
                        string macadresstelnet = Regex.Replace(MAC.Replace(" ", ""), "(.{2})(?!$)", "$0-").ToLower();
                        String[] position = sendCommand(listaConexiones[IPOLT_ONU], "show onu-position " + macadresstelnet);
                        foreach (String pos in position)
                        {
                            if (pos.Contains("Found ONU"))
                            {
                                String[] posSep = { "(", ")" };
                                String[] posAux = pos.Split(posSep, StringSplitOptions.RemoveEmptyEntries);
                                Console.WriteLine(posAux[1]);

                                String[] portPosSep = { "-" };
                                String[] portPos = posAux[1].Split(portPosSep, StringSplitOptions.RemoveEmptyEntries);
                                string puerto = portPos[1];
                                string posicion = portPos[2];

                                if (posAux[2].Contains("online"))
                                {
                                    // Aqui va el codigo cuando está online
                                    String[] sep3 = { "-", ")" };
                                    String[] NumeroONUAux = pos.Split(sep3, StringSplitOptions.RemoveEmptyEntries);

                                    sendCommand(listaConexiones[IPOLT_ONU], "olt " + NumeroONUAux[1]);
                                    sendCommand(listaConexiones[IPOLT_ONU], "onu " + NumeroONUAux[2]);
                                    sendCommand(listaConexiones[IPOLT_ONU], "catv enable");
                                    sendCommand(listaConexiones[IPOLT_ONU], "exit");
                                    sendCommand(listaConexiones[IPOLT_ONU], "exit");
                                    sendCommand(listaConexiones[IPOLT_ONU], "system save all");

                                    DBFuncion db2 = new DBFuncion();
                                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Ok");
                                    db2.consultaSinRetorno("ActualizaComandos");
                                    return;
                                }
                            }
                         }
                    }
                }
                else
                {
                    Console.WriteLine("Si esta en la lista de ilegales ,en OLT " + IPOLT_ONU);
                    byte[] MacOctetString = MAC.Split(' ').Select(x => Convert.ToByte(x, 16)).ToArray();
                    Console.WriteLine("Agregando mac a la whitelist");
                    AddMacToWhiteList(MacOctetString, BuscarIdIlegales(MAC, IPOLT_ONU), ContarMacWhiteList(BuscarIdIlegales(MAC, IPOLT_ONU), IPOLT_ONU), IPOLT_ONU);

                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 0);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "esperando activacion tv");
                    db2.consultaSinRetorno("ActualizaComandos");
                }  
                
            }
            catch (Exception ex)
            {
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, Consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, ex.Message);
                db2.consultaSinRetorno("ActualizaComandos");
                if (comandosProcesados.Any(t => t.Consecutivo == Consecutivo))
                {
                    comandosProcesados.Where(t => t.Consecutivo == Consecutivo).FirstOrDefault().Procesado = true;
                }
            }
        }


    }
}
