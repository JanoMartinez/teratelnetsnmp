﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazTelnet
{
    /// <summary>
    /// Clase de Manejo de la información de las OLT almacenadas en la tabla GENERALS de la base de datos
    /// </summary>
    public class OLT
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string User_ { get; set; }
        public string Pass { get; set; }
    }

    public class Comandos
    {
        public long Consecutivo { get; set; }
        public int Contrato { get; set; }
        public string MacAddress { get; set; }
        public string MacOctetString { get; set; }
        public string Comando { get; set; }
        public int Resultado { get; set; }
        public int Puerto { get; set; }
        public int NoOnu { get; set; }
        public string IP { get; set; }
        public DateTime Fecha_Habilitar { get; set; }
        public string Velocidad_Bajada { get; set; }
        public string Velocidad_Subida { get; set; }
        public int Prioridad { get; set; }
        public int OS { get; set; }
        public int idFibra { get; set; }
        public int Vlan { get; set; }
        public int tipserv { get; set; }
        public bool Whitelist { get; set; }
    }

    public class ComandoProcesado
    {
        public long Consecutivo { get; set; }
        public bool Procesado { get; set; }
    }
}
